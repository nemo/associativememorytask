function [SubjectInfo] = GetSubjectInfo(ResultsDir)
Title = 'Memory Task';

%%%% SETTING DIALOG OPTIONS
% Options.WindowStyle = 'modal';
Options.Resize = 'on';
Options.Interpreter = 'tex';
Options.CancelButton = 'on';
Options.ApplyButton = 'off';
Options.ButtonNames = {'Continue','Cancel'}; 
Option.Dim = 8; % Horizontal dimension in fields

Prompt = {};
Formats = {};
DefAns = struct([]);

Prompt(1,:) = {'Subject Number', 'Number',[]};
Formats(1,1).type = 'edit';
Formats(1,1).format = 'text';
Formats(1,1).size = 30; % automatically assign the height
DefAns(1).Number = '';


Prompt(2,:) = {'Initials', 'Initials',[]};
Formats(2,1).type = 'edit';
Formats(2,1).format = 'text';
Formats(2,1).size = 80;
DefAns.Initials = '';


Prompt(3,:) = {'Type','Type',[]};
Formats(3,1).type = 'list';
Formats(3,1).format = 'text';
Formats(3,1).style = 'radiobutton';
Formats(3,1).items = {'Task' 'Practice'};
DefAns.Type = 'Task';%3; % 


Prompt(4,:) = {'Session', 'Session','(session number)'};
Formats(4,1).type = 'edit';
Formats(4,1).format = 'integer';
Formats(4,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(4,1).size = 80;
Formats(4,1).unitsloc = 'bottomleft';
DefAns.Session = '';


Prompt(5,:) = {'Run', 'Run','(run number; use 0 for Practice)'};
Formats(5,1).type = 'edit';
Formats(5,1).format = 'integer';
Formats(5,1).limits = [0 999999999]; % 9-digits (positive #)
Formats(5,1).size = 80;
Formats(5,1).unitsloc = 'bottomleft';
DefAns.Run = 1;


Prompt(6,:) = {'Task Stages','Stages',[]};
Formats(6,1).type = 'list';
Formats(6,1).format = 'text';
Formats(6,1).style = 'radiobutton';
Formats(6,1).items = {'Encode + Recall' 'Encode Only' 'Recall Only'};
DefAns.Stages = 'Encode + Recall';

[Answer,Cancelled] = inputsdlg(Prompt,Title,Formats,DefAns,Options);

if (Cancelled)
    error(' ---- Cancel button pressed -----');
    return;
end

SUBJECT = Answer.Number;
Session = Answer.Session;
Run = Answer.Run;

cTask = clock; %Current date and time as date vector. [year month day hour minute seconds]
baseNameTask=[SUBJECT '_S_' num2str(Session) '_MemTask_' num2str(cTask(3)) '_' num2str(cTask(2)) '_' num2str(cTask(1)) '_' num2str(cTask(4)) '_' num2str(cTask(5))]; %makes unique filename

%% Create directory to save Data

%SubjectInfo.infoTask = infoTask;
SubjectInfo.Subject = SUBJECT;
SubjectInfo.Session = Session;
SubjectInfo.Run = Run;
SubjectInfo.cTask = cTask;
SubjectInfo.baseNameTask = baseNameTask;

SubjFolder = ([ResultsDir filesep 'Subject_' num2str(SUBJECT)]);

% Create Subject folder
if ~exist(SubjFolder, 'dir')
    mkdir(SubjFolder);
end

% % Create Parameters folder
% 
% ParamFolder = ([SubjFolder filesep 'Parameters' ]);
% if ~exist(ParamFolder, 'dir')
%     mkdir(ParamFolder);
% end
% SubjectInfo.ParamFolder = ParamFolder;

% Create session folder
SessionFolder = ([SubjFolder filesep 'Session_' num2str(Session)]);
if ~exist(SessionFolder, 'dir')
    mkdir(SessionFolder);
end

% Create Run folder
RunFolder = ([SessionFolder filesep 'Run_' num2str(Run)]);
if ~exist(RunFolder, 'dir')
    mkdir(RunFolder);
end

SubjectInfo.SubjFolder = SubjFolder;
SubjectInfo.SessionFolder = SessionFolder;
SubjectInfo.RunFolder = RunFolder;
if strcmp(Answer.Stages,'Encode Only'); encode = true; end
if strcmp(Answer.Stages,'Encode + Recall'); encode = true; end
if strcmp(Answer.Stages,'Recall Only'); encode = false; end
if strcmp(Answer.Stages,'Recall Only'); recall = true; end
if strcmp(Answer.Stages,'Encode + Recall'); recall = true; end
if strcmp(Answer.Stages,'Encode Only'); recall = false; end


SubjectInfo.Encode = encode;
SubjectInfo.Recall = recall;
SubjectInfo.Type = Answer.Type;
SubjectInfo.TaskStages = Answer.Stages;

%% Add Task Parameters
if strcmp(SubjectInfo.Type,'Task')
    SubjectInfo.TotalTrials = 84; %120 % This is the total number of trials for the task, across all stimulation conditions
    SubjectInfo.NumConditions = 3; % This is the number of stimulation conditions; e.g. 3 freq offsets: sham, excitatory, inhibitory
elseif strcmp(SubjectInfo.Type,'Practice')
    SubjectInfo.TotalTrials = 6; %120 % This is the total number of trials for the task, across all stimulation conditions
    SubjectInfo.NumConditions = 1;
end

