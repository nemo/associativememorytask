function [stim] = StimuliAllocation(TaskDir, SubjectInfo)

%% Allocate stimuli
% Each trial starts with a presentation of a face + object
% Recall has the face + 3 objects (1 target + 1 foil + 1 distractor)
% Here need to allocate the faces, target object and distractor per face

%% Clear the workspace and the screen
rand('state',sum(100*clock));

%% Specify stimuli directories
%TaskDir = ('~/Dropbox/TI_CZ-UK/AssociativeTask'); % flag
FacesDir = ([TaskDir filesep 'resources' filesep 'faces' filesep 'Images' filesep 'CFD']);
ObjectsDir = ([TaskDir filesep 'resources' filesep 'objects']);

addpath(genpath([TaskDir filesep 'functions']));

%% Parameters
TotalTrials = SubjectInfo.TotalTrials;
NumConditions = SubjectInfo.NumConditions;
FaceCategory = 8;
%NumConditions = 3; % flag - this will be the number of experimental conditions
remo = rem(TotalTrials,NumConditions);
if remo>0
    warning('Total number of trials is not divisible by the number of conditions - stimulation conditions will be unbalanced!')
end

fbalance = rem((TotalTrials/FaceCategory)/NumConditions,1);
if fbalance>0
    warning('Cannot create a fully balanced face allocation design')
end

%% Define Faces
% Check number of pictures per folder
AF = dir([FacesDir filesep 'AF*' filesep '*N.jpg']); %1
AM = dir([FacesDir filesep 'AM*' filesep '*N.jpg']); %2
BF = dir([FacesDir filesep 'BF*' filesep '*N.jpg']); %3
BM = dir([FacesDir filesep 'BM*' filesep '*N.jpg']); %4
LF = dir([FacesDir filesep 'LF*' filesep '*N.jpg']); %5
LM = dir([FacesDir filesep 'LM*' filesep '*N.jpg']); %6
WF = dir([FacesDir filesep 'WF*' filesep '*N.jpg']); %7
WM = dir([FacesDir filesep 'WM*' filesep '*N.jpg']); %8

%% Create stimuli table

stimuli = table;
stimuli.trialaux = (1:TotalTrials)';

% Create an array for the conditions

order_tmp = repmat(1:NumConditions,[1 round(TotalTrials/NumConditions,0)]);
if remo>0
    order_tmp = [order_tmp randperm(NumConditions,remo)];
end


% Define face type per condition
order_face =  repelem((1:FaceCategory),round(((TotalTrials/FaceCategory)/NumConditions),0));
order_faces = repmat(order_face, 1,NumConditions);
order_faces = order_faces(1:TotalTrials);

% Select one object per face for target and distractor
objects = dir([ObjectsDir filesep '*.jpg']);
objectsuse = randperm(size(objects,1),(TotalTrials*4)); % multiplied by 4 because half will be targets and the other half distractors
objtarget = objectsuse(1:2:TotalTrials*2);
objdist = objectsuse(2:2:TotalTrials*2);

% Create array with the prepared variables
tmp = sort(order_tmp)'; % 1st column is stim condition
tmp(:,2) = order_faces'; % 2nd column is face condition
tmp(:,3) = objtarget';
tmp(:,4) = objdist';

% Add paths for each image
% Faces
sstruct = [AF; AM; BF; BM; LF; LM; WF; WM];
idx = [ones(length(AF),1); ones(length(AM),1)*2; ones(length(BF),1)*3; ones(length(BM),1)*4;...
    ones(length(LF),1)*5; ones(length(LM),1)*6; ones(length(WF),1)*7; ones(length(WM),1)*8];
allfaces = struct2table(sstruct);
allfaces.facetype = idx;

fcat = tmp(:,2);
counter = 0;
for ii=1:FaceCategory
    n = sum(ismember(fcat,ii));
    s = randperm(sum(allfaces.facetype==ii),n);
    if ii==1
        selc = allfaces(s,:);
    else
        counter = counter + (sum(allfaces.facetype==ii-1));
        sinc = s+counter;
        selc = vertcat(selc, allfaces(sinc,:));
    end
end


stimuli.stimcond = tmp(:,1);
stimuli.facetype = tmp(:,2);
stimuli.targetnumber = tmp(:,3);
stimuli.distractornumber = tmp(:,4);

stimuli = sortrows(stimuli,"facetype","ascend");
selc.facetype = [];
stim = [stimuli,selc];
stim(:,FaceCategory:end) = [];

% Objects

for ii=1:height(stim)
    targetname{ii,:} = objects(stim.targetnumber(ii)).name;
    targetfolder{ii,:} = [objects(stim.targetnumber(ii)).folder filesep objects(stim.targetnumber(ii)).name];

    distractorname{ii,:} = objects(stim.distractornumber(ii)).name;
    distractorfolder{ii,:} = [objects(stim.distractornumber(ii)).folder filesep objects(stim.distractornumber(ii)).name];
end

stim.targetname = targetname;
stim.targetfolder = targetfolder;
stim.distractorname = distractorname;
stim.distractorfolder = distractorfolder;

%% Reorder stimuli based on some rules - here just making sure that no more than 3 stimuli of the same stimulation condition in a row
pass=0;
while pass<1
    clear idx
    aux = stim.stimcond;
    idx = randperm(numel(aux));
    aux = tmp(idx,:);
    seqones = findseq(aux(:,1)');
    oneseq=find(seqones(:,1)==1);
    maxseqones=max(seqones(oneseq,4));
    if maxseqones<3 % Ensure no more than 3 of the same type in a row
        pass=1;
    end
end
stim=stim(idx,:);
stim.trialaux = [];
stim.trial = (1:TotalTrials)';
stim = movevars(stim,'trial','Before','stimcond');

% Add a foil per trial - only rule is that the foil cannot match the target
pass=0;
targetn = stim.targetnumber;
while pass<1
    idx = targetn(randperm(numel(targetn)));
    if any(targetn==idx)
        pass=0;
    else
        pass=1;
    end
end

stim.foilnumber = idx;
for ii=1:height(stim)
    foilname{ii,:} = objects(stim.foilnumber(ii)).name;
    foilfolder{ii,:} = [objects(stim.foilnumber(ii)).folder filesep objects(stim.foilnumber(ii)).name];
end
stim.foilname = foilname;
stim.foilfolder = foilfolder;

% Define recall position
pass=0;
while pass<1
    OrderResp = randperm(numel(1:TotalTrials));
    y = intersect(OrderResp(end-1:end),(1:2)); % last two encode cannot be the 1st or 2nd to recall
    if isempty(y)
        pass=1;
    end
end

stim.orderresp = OrderResp';




end
