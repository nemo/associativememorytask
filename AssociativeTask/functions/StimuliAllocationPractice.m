function [stim, TotalTrials] = StimuliAllocationPractice(TaskDir, SubjectInfo)
%% Allocate stimuli for the task practice

%% Clear the workspace and the screen
rand('state',sum(100*clock));

%% Specify stimuli directories
FacesDir = ([TaskDir filesep 'resources' filesep 'practice' filesep 'faces']);
ObjectsDir = ([TaskDir filesep 'resources' filesep 'practice' filesep 'objects']);

addpath(genpath([TaskDir filesep 'functions']));

%% Parameters
TotalTrials = SubjectInfo.TotalTrials; 

%% Create stimuli table

stim = table;
stim.trial = (1:TotalTrials)';

faces = dir([FacesDir filesep '*N.jpg']);
facesuse = randperm(size(faces,1),(TotalTrials));
allfaces = struct2table(faces(facesuse));
stim.name = allfaces.name;
stim.folder = allfaces.folder;

% Select one object per face for target and distractor
objects = dir([ObjectsDir filesep '*.jpg']);
objectsuse = randperm(size(objects,1),(TotalTrials*4)); % multiplied by 4 because half will be targets and the other half distractors
objtarget = objectsuse(1:2:TotalTrials*2);
objdist = objectsuse(2:2:TotalTrials*2);

%
stim.targetnumber = objtarget';
stim.distractornumber = objdist';

% Objects

for ii=1:height(stim)
    targetname{ii,:} = objects(stim.targetnumber(ii)).name;
    targetfolder{ii,:} = [objects(stim.targetnumber(ii)).folder filesep objects(stim.targetnumber(ii)).name];

    distractorname{ii,:} = objects(stim.distractornumber(ii)).name;
    distractorfolder{ii,:} = [objects(stim.distractornumber(ii)).folder filesep objects(stim.distractornumber(ii)).name];
end


stim.targetname = targetname;
stim.targetfolder = targetfolder;
stim.distractorname = distractorname;
stim.distractorfolder = distractorfolder;

% Add a foil per trial - only rule is that the foil cannot match the target
pass=0;
targetn = stim.targetnumber;
while pass<1
    idx = targetn(randperm(numel(targetn)));
    if any(targetn==idx)
        pass=0;
    else
        pass=1;
    end
end

stim.foilnumber = idx;
for ii=1:height(stim)
    foilname{ii,:} = objects(stim.foilnumber(ii)).name;
    foilfolder{ii,:} = [objects(stim.foilnumber(ii)).folder filesep objects(stim.foilnumber(ii)).name];
end
stim.foilname = foilname;
stim.foilfolder = foilfolder;

% Define recall position
pass=0;
while pass<1
    OrderResp = randperm(numel(1:TotalTrials));
    y = intersect(OrderResp(end-1:end),(1:2)); % last two encode cannot be the 1st or 2nd to recall
    if isempty(y)
        pass=1;
    end
end

stim.orderresp = OrderResp';



end
