%% Associative memory task

% ------------ Task Description -------------------------------------------
% The task includes an encoding stage and a recall stage
% Durind the encoding a face is presented alongside an object - the
% participant is asked to memorise the two items together, by creating an
% association between the two (for example, trying to create a story that
% binds them together).
% During the recall period the participants are shown all faces they saw
% before and 3 objects: target (the matching item they saw during encoding),
% foil (an object that was paired with a different face),
% distractor (an object that was not presented before).

% -------------------------------------------------------------------------
% Ines Violante, Aug 2024
% v1

%% Clear the workspace and the screen
sca;
close all;
clear

%% Go to task directory
TaskDir = ('~/Dropbox/TI_CZ-UK/AssociativeTask');  % Change this path to match yours!
ResultsDir =  ([TaskDir filesep 'Data']);
addpath(genpath(TaskDir));

cd (TaskDir)

%% Task Parameters
EncDuration = 4;   % Duration of the presentation of each item during encoding
MaxRecDuration = 10; % Maxium time it waits for a response

debugg = 0; % put to zero for testing; This is to stop other keys except for the selected keys to be active and remove mouse arrow

%% Subject Info, Directories, Additional Task Parameters
[SubjectInfo] = GetSubjectInfo(ResultsDir);

%% Create the task array populated with the order of trials and stimuli to be shown
pathname = SubjectInfo.RunFolder;
pathdir = dir([SubjectInfo.RunFolder filesep '*.mat']);


if (SubjectInfo.Encode)
    if strcmp(SubjectInfo.Type,'Practice')
        [stim] = StimuliAllocationPractice(TaskDir, SubjectInfo);
    else
        [stim] = StimuliAllocation(TaskDir, SubjectInfo);
    end
     matfile = fullfile(pathname, SubjectInfo.baseNameTask);

else % Load parameters
    if isempty(pathdir)
        warning('probably there is no .mat file in the run folder')
    end
    load([pathdir.folder filesep pathdir.name])
    SubjectInfo.Encode = false;
    SubjectInfo.Recall = true;
    % Append the file with recall only and date
 
    cTask = clock; %Current date and time as date vector. [year month day hour minute seconds]
    baseNameTask=['_RecallOnly_' num2str(cTask(3)) '_' num2str(cTask(2)) '_' num2str(cTask(1)) '_' num2str(cTask(4)) '_' num2str(cTask(5))]; %makes unique filename

    newname = ([SubjectInfo.baseNameTask, baseNameTask]);
    matfile = fullfile(pathname, newname);
end

TotalTrials = SubjectInfo.TotalTrials;
NumConditions = SubjectInfo.NumConditions;

% Generate ISI distribution 
p = 6;
xmin=1.5;
xmax=2.5;
ISI = xmin + (xmax - xmin)*sum(rand(SubjectInfo.TotalTrials,p),2)/p;



%% Screen Parameters

% Enable alpha blending with proper blend-function
AssertOpenGL;

% Check screens to see if dual display is possible
screens=Screen('Screens');
screenNumber=max(screens);

Screen('Preference', 'SkipSyncTests', 2)  % Change to zero for actual experiment
white=WhiteIndex(screenNumber);
black=BlackIndex(screenNumber);
grey = white / 2;

%[window screenRect]=Screen('OpenWindow',screenNumber, white);
[window,screenRect] = PsychImaging('OpenWindow', screenNumber, grey);
Screen('BlendFunction', window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', window);

% Query the frame duration
ifi = Screen('GetFlipInterval', window);

% Get the centre coordinate of the window
[xCenter, yCenter] = RectCenter(screenRect);

% Sync us and get a time stamp
vbl = Screen('Flip', window);
waitframes = 1;

% Maximum priority level
topPriorityLevel = MaxPriority(window);
Priority(topPriorityLevel);

%% Define position for the images to appear

% image positions for encoding
rect1_enc = CenterRectOnPointd([0 0 xCenter/2 xCenter/2], xCenter-(xCenter/3), yCenter)';
rect2_enc = CenterRectOnPointd([0 0 xCenter/2 xCenter/2], xCenter+(xCenter/3), yCenter)';
rect_enc = [rect1_enc rect2_enc];

% Determine the posicion of images during the encoding stage - right or
% left
posenc = (repmat(1:2,1,TotalTrials/2));
posenc = posenc(randperm(numel(posenc)));

% image positions for recall
rect1 = CenterRectOnPointd([0 0 xCenter/3 xCenter/3], xCenter, yCenter-(yCenter/2))';  % Top image
rect2 = CenterRectOnPointd([0 0 xCenter/3 xCenter/3], xCenter, yCenter+(yCenter/3))'; % middle bottom
rect3 = CenterRectOnPointd([0 0 xCenter/3 xCenter/3], xCenter-(xCenter/2), yCenter+(yCenter/3))'; %left bottom
rect4 = CenterRectOnPointd([0 0 xCenter/3 xCenter/3], xCenter+(xCenter/2), yCenter+(yCenter/3))'; %right bottom
rect_recall = [rect3 rect2 rect4];


%% Keyboard Parameters

% use keyboard to collect task data
deviceIndex = [];
KbName('UnifyKeyNames');

escapeKey = KbName('q');
respKey = KbName('SPACE');
leftKey = KbName('LeftArrow');
rightKey = KbName('RightArrow');
tkey = KbName('s');

keysOfInterest=zeros(1,256);
keysOfInterest(escapeKey)=1;
keysOfInterest(rightKey)=1;
keysOfInterest(leftKey)=1;
keysOfInterest(respKey)=1;

%% Text Parameters
TextSize = 60;
Screen('TextFont', window, 'Helvetica');
Screen('TextSize', window, TextSize);

%% Fixation Cross Parameters
fixCrossDimPix = 50;
lineWidthPix = 7;

xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
allCoords = [xCoords; yCoords];


%% --------- Task Loop --------------------

if ~debugg
    HideCursor;
    ListenChar(-1); %makes it so characters typed don?t show up in the command window  - remove if testing
end


Screen('TextSize', window, TextSize);  %%%%%%%%%%%%%%%%%
DrawFormattedText(window, 'Memory Task', 'center', 'center', white);
DrawFormattedText(window, 'Get Ready!', 'center', yCenter+100, white);
Screen('TextSize', window, 30);
DrawFormattedText(window, 'press s to start', 'center', yCenter+300, white);

Screen(window, 'Flip');
[keyisdown,secs,keycode] = KbCheck;
while(~keycode(tkey))       % wait for key to be pressed to start or use scanner key
    [keyisdown,secs,keycode] = KbCheck;
    WaitSecs(0.001); % delay to prevent CPU hogging
end

% -------- Encode Period  -----------------

StructTask = [];
counter = 1;
TimeTaskStart = GetSecs;

if (SubjectInfo.Encode)
    for ii = 1:TotalTrials

        TimeTrialStart = GetSecs()-TimeTaskStart;
        TimeEncStarted = GetSecs();

        % -------- Encode period ------------------------
        % Face
        facefilename = strjoin([stim.folder(ii) filesep stim.name(ii)],'');
        facedata = imread(char(facefilename));
        [imageHeight, imageWidth, colorChannels] = size(facedata);
        face=Screen('MakeTexture',window,facedata);

        % Object
        targetfilename = char(stim.targetfolder(ii));
        targetdata = imread(char(targetfilename));
        [imageHeight, imageWidth, colorChannels] = size(targetdata);
        target=Screen('MakeTexture',window,targetdata);

        % if 1 face is left if 2 is to the right
        if posenc(ii)==1
            Screen('DrawTexture', window, face,[],rect1_enc);
            Screen('DrawTexture', window, target,[],rect2_enc);
        else
            Screen('DrawTexture', window, face,[],rect2_enc);
            Screen('DrawTexture', window, target,[],rect1_enc);
        end

        Screen(window, 'Flip');
        TimeEncStimuliStarted = GetSecs()-TimeTaskStart;

        while (GetSecs - TimeEncStarted <= EncDuration)
            % break task if needed
            [keyisdown,secs,keycode] = KbCheck;
            if(keycode(escapeKey))
                Screen('CloseAll');
                sca;
                ShowCursor;
                ListenChar(0);
                break;
            end
        end
        TimeEncStimuliEnded = GetSecs()-TimeTaskStart;

        StructTask(counter).trial = ii;
        StructTask(counter).TimeTrialStart = TimeTrialStart;
        StructTask(counter).TimeEncStimuliStarted = TimeEncStimuliStarted;
        StructTask(counter).TimeEncStimuliEnded = TimeEncStimuliEnded;
        StructTask(counter).facestimuli = stim.name(ii);
        StructTask(counter).facefolder = char(facefilename);
        StructTask(counter).objtargetstimuli = stim.targetname(ii);
        StructTask(counter).objtargetfolder = char(stim.targetfolder(ii));
        StructTask(counter).facelocation = posenc(ii); %if 1 face is left if 2 is to the right


        % -------- ISI period ------------------------
        Screen('DrawLines', window, allCoords,lineWidthPix, white, [xCenter yCenter], 2);
        Screen('Flip',window);
        TimeFixPresented = GetSecs()-TimeTaskStart;
        TimeISIStart = GetSecs();
        while (GetSecs - TimeISIStart <= ISI(ii))
            [keyisdown,secs,keycode] = KbCheck;
            if(keycode(escapeKey))
                Screen('CloseAll');
                sca;
                ShowCursor;
                ListenChar(0);
                break;
            end
        end

        StructTask(counter).ISIstart = TimeFixPresented;
        StructTask(counter).ISI = ISI(ii);
        counter = counter+1;

        %% Save Information
        save (matfile, 'SubjectInfo', 'StructTask', 'stim');
    end
end



% -------- Recall Period  -----------------
if (SubjectInfo.Recall)
for ii = 1:TotalTrials
    TimeRecTrialStart = GetSecs()-TimeTaskStart;
    TimeRecStarted = GetSecs();

    resp_idx = find(stim.orderresp==ii);

    % Face appears at the top, 3 object options to select from
    facefilename = strjoin([stim.folder(resp_idx) filesep stim.name(resp_idx)],'');
    facedata = imread(char(facefilename));
    [imageHeight, imageWidth, colorChannels] = size(facedata);
    face=Screen('MakeTexture',window,facedata);

    % Object target
    targetfilename = char(stim.targetfolder(resp_idx));
    targetdata = imread(char(targetfilename));
    [imageHeight, imageWidth, colorChannels] = size(targetdata);
    target=Screen('MakeTexture',window,targetdata);


    % Distractor
    distfilename = char(stim.distractorfolder(resp_idx));
    distdata = imread(char(distfilename));
    [imageHeight, imageWidth, colorChannels] = size(distdata);
    distractor=Screen('MakeTexture',window,distdata);

    % Foil
    foilfilename = char(stim.foilfolder(resp_idx));
    foildata = imread(char(foilfilename));
    [imageHeight, imageWidth, colorChannels] = size(foildata);
    foil=Screen('MakeTexture',window,foildata);

    posrec = randperm(3);  % 3 options - target, foil, distractor
    objrecall = [target foil distractor]; %make an vector of textsure pointers
    objrecalls = objrecall(posrec);
    trgpos = find(posrec==1);
    foilpos = find(posrec==2);
    distpos = find(posrec==3);
    currentsq = 1;

    responded = NaN;
    trgselec = NaN;
    distselc = NaN;
    foilselc = NaN;
    RT = NaN;
    RTonset = NaN;
    press1 = 0;
    rpress = 0;
    lpress = 0;
    rtonset = 0;
    keysOfInterest=zeros(1,256);
    keysOfInterest(rightKey)=1;
    keysOfInterest(leftKey)=1;
    keysOfInterest(respKey)=1;

    KbQueueCreate(deviceIndex, keysOfInterest);
    KbQueueStart(deviceIndex);

    % Define where each image will be presented
    TimeResponseStart = GetSecs;
    while GetSecs - TimeRecStarted <= MaxRecDuration
        Screen('DrawTexture', window, face,[],rect1);
        Screen('DrawTextures', window, objrecalls,[],rect_recall);

        % Move square depending on key press
        %[keyisdown,secs,keycode] = KbCheck;
        [pressed, firstPress]=KbQueueCheck(deviceIndex); % so it only does one keypress at a time
        if pressed
            if press1 == 0
                if rtonset == 0
                    FirstButtonPress = GetSecs();
                    rtonset = 1;
                end
                press1 = 1;
            end
            if firstPress(rightKey)
                currentsq = currentsq + 1;
                rpress = rpress + 1;
            elseif firstPress(leftKey)
                currentsq = currentsq - 1;
                lpress = lpress + 1;
            end
        end
        if currentsq < 1
            currentsq = 1;
        end


        if currentsq > size(rect_recall,2)
            currentsq = 1;
        end

        Screen('FrameRect', window , [255 255 0], rect_recall(:,currentsq), 8);
        Screen('Flip', window)

        % Get Response
        WaitSecs(0.004)
        if pressed
            if firstPress(respKey)
                if  rtonset == 1
                    RTonset = GetSecs() - FirstButtonPress; % For the cases where only the respKey was pressed
                else
                    RTonset = GetSecs() - TimeResponseStart;
                end

                RT = GetSecs() - TimeResponseStart;
                responded=currentsq;
                if responded==trgpos
                    trgselec = 1;
                    distselc = 0;
                    foilselc = 0;
                elseif responded==distpos
                    trgselec = 0;
                    distselc = 1;
                    foilselc = 0;
                elseif responded==foilpos
                    trgselec = 0;
                    distselc = 0;
                    foilselc = 1;
                end
            end
        end

        if responded>0
            Screen('Flip', window, [], 0);
            WaitSecs(0.2);
            break;
        end

        [keyisdown,secs,keycode] = KbCheck;
        if(keycode(escapeKey))
            Screen('CloseAll');
            sca;
            ShowCursor;
            ListenChar(0);
            break;
        end

    end
    if responded==0
        currentsq = NaN;
    end
    KbQueueRelease(deviceIndex);
    Screen('Close');

    StructTask(resp_idx).RespOrder = stim.orderresp(ii);
    StructTask(resp_idx).TimeRecTrialStart = TimeRecTrialStart;
    StructTask(resp_idx).targetfilename  = targetfilename;
    StructTask(resp_idx).distfilename = distfilename;
    StructTask(resp_idx).targetpos = find(posrec==1);
    StructTask(resp_idx).foilpos = find(posrec==2);
    StructTask(resp_idx).distpos = find(posrec==3);
    StructTask(resp_idx).RTonset = RTonset;
    StructTask(resp_idx).responded = responded;
    StructTask(resp_idx).target = trgselec;
    StructTask(resp_idx).foil = foilselc;
    StructTask(resp_idx).distractor = distselc;
    StructTask(resp_idx).RT = RT;
    StructTask(resp_idx).n_rpress = rpress;
    StructTask(resp_idx).n_lpress = lpress;

    %% Save Information
    save (matfile, 'SubjectInfo', 'StructTask', 'stim');

end
end

 %% Save Information
 save (matfile, 'SubjectInfo', 'StructTask', 'stim');

%% Close all
DrawFormattedText(window, 'Thank you', 'center', 'center', white);
Screen(window, 'Flip');
WaitSecs(2)
Screen('CloseAll');
sca;
ShowCursor;





